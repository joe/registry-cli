# Copyright 2019 Jorrit Fahlke <jorrit@jorrit.de>
#
# SPDX-License-Identifier: GPL-2.0-or-later

variables:
  REGISTRY: registry.dune-project.org
  JOB_REPO: $REGISTRY/$CI_PROJECT_PATH/$CI_JOB_NAME
  PIPELINE_TAG: $CI_COMMIT_REF_SLUG.$CI_PIPELINE_IID

stages:
- utils
- build
- test
- deploy

######################################################################
#
#  Utils: save scripts in artifacts needed for stopping environments
#

utils:
  stage: utils
  image: alpine:latest
  tags:
  - hpc2seci
  script:
  - echo "Nothing to do..."
  artifacts:
    paths:
    - bin/

######################################################################
#
#  Build: build docker images and define how to stop environments
#

.build: &build
  stage: build
  image: docker:stable
  services:
  - docker:dind
  before_script:
  - apk add bash curl jq
  - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $REGISTRY
  tags:
  - hpc2seci-dind
  dependencies: []
  script:
  - docker pull "$JOB_REPO:$PIPELINE_TAG" || true
  - docker pull "$JOB_REPO:$CI_COMMIT_REF_SLUG" || true
  - docker pull "$JOB_REPO:master" || true
  - docker build --pull --build-arg FROM="$FROM" --cache-from "$JOB_REPO:$PIPELINE_TAG" --cache-from "$JOB_REPO:$CI_COMMIT_REF_SLUG" --cache-from "$JOB_REPO:master" -t "$JOB_REPO:$PIPELINE_TAG" .
  - bin/trash-temps "$CI_JOB_NAME"
  - docker push "$JOB_REPO:$PIPELINE_TAG"
  environment:
    name: tmp/$CI_COMMIT_REF_SLUG
    on_stop: stop-build

alpine:
  <<: *build
  variables:
    FROM: alpine:latest

docker:
  <<: *build
  variables:
    FROM: docker:stable

stop-build:
  stage: build
  image: alpine:latest
  tags:
  - hpc2seci
  variables:
    GIT_STRATEGY: none
  dependencies:
  - utils
  script:
  - apk add bash curl jq
  - bin/trash-temps alpine docker
  when: manual
  environment:
    name: tmp/$CI_COMMIT_REF_SLUG
    action: stop
  except:
  - master

######################################################################
#
#  Test: check whether we can list ourselves from our own registry
#

test-tags:
  stage: test
  image: $REGISTRY/$CI_PROJECT_PATH/docker:$PIPELINE_TAG
  services:
  - name: docker:dind
    command: ["--insecure-registry=0.0.0.0/0"]
  - name: registry:2
  tags:
  - hpc2seci-dind
  dependencies: []
  script:
  - registry=($(getent hosts registry))
  - docker build -t $registry:5000/testimage:test -f Dockerfile.test .
  - docker push $registry:5000/testimage:test
  - result=$(registry-cli --insecure tag ls registry:5000/testimage)
  - printf '%s\n' "$result"
  - "[[ $result == test ]]"

test-tag-cp:
  stage: test
  image: $REGISTRY/$CI_PROJECT_PATH/docker:$PIPELINE_TAG
  services:
  - name: docker:dind
    command: ["--insecure-registry=0.0.0.0/0"]
  - name: registry:2
  tags:
  - hpc2seci-dind
  dependencies: []
  script:
  - registry=($(getent hosts registry))
  - docker build -t $registry:5000/testimage:test -f Dockerfile.test .
  - docker push $registry:5000/testimage:test
  - registry-cli --insecure tag cp registry:5000/testimage:test registry:5000/testimage:test2
  - docker pull $registry:5000/testimage:test2
  - test_id=$(docker image ls --format "{{.ID}}" $registry:5000/testimage:test)
  - "[[ -n $test_id ]]"
  - test2_id=$(docker image ls --format "{{.ID}}" $registry:5000/testimage:test2)
  - "[[ -n $test2_id ]]"
  - "[[ $test_id = $test2_id ]]"

######################################################################
#
#  Deploy: rename the temporary images to the final ones.
#

deploy:
  stage: deploy
  image: alpine:latest
  tags:
  - hpc2seci
  variables:
    GIT_STRATEGY: none
  dependencies:
  - utils
  script:
  - apk add bash curl jq
  - bin/registry-cli tag cp "$REGISTRY/$CI_PROJECT_PATH/alpine:$PIPELINE_TAG" "$REGISTRY/$CI_PROJECT_PATH/alpine:$CI_COMMIT_REF_SLUG"
  - bin/registry-cli tag cp "$REGISTRY/$CI_PROJECT_PATH/docker:$PIPELINE_TAG" "$REGISTRY/$CI_PROJECT_PATH/docker:$CI_COMMIT_REF_SLUG"
  environment:
    name: $CI_COMMIT_REF_SLUG
    on_stop: stop

stop:
  stage: deploy
  image: alpine:latest
  tags:
  - hpc2seci
  variables:
    GIT_STRATEGY: none
  dependencies:
  - utils
  script:
  - apk add bash curl jq
  - bin/registry-cli untag --trash "$REGISTRY/$CI_PROJECT_PATH/alpine:$CI_COMMIT_REF_SLUG"
  - bin/registry-cli untag --trash "$REGISTRY/$CI_PROJECT_PATH/docker:$CI_COMMIT_REF_SLUG"
  when: manual
  environment:
    name: $CI_COMMIT_REF_SLUG
    action: stop
  except:
  - master
