# Copyright 2019 Jorrit Fahlke <jorrit@jorrit.de>
#
# SPDX-License-Identifier: GPL-2.0-or-later

ARG FROM
FROM $FROM

RUN apk add bash curl jq
COPY bin/registry-cli /usr/local/bin/
